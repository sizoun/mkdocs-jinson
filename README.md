# mkdocs-jinson

Un simple plugin pour mkdocs permettant de parser un ficher `.json` en variables jinja.

## Installation

Télécharger et glisser les dossiers `mkdocs_jinson_plugin` et `MkDocsJinsonPlugin-0.1.dist-info` à la racine du dossier contenant les plugins mkdocs.

## Appel plugin

Dans le fichier mkdocs.yml du dossier du projet mkdocs, appeler le plugin et déclarer les fichiers que l'on souhaite parser. :


```
plugins:
  - jinson:
      json_path: 
        - 'path/file.json', 'path/file2.json'
```

## Dans page.md

Pour récupérer le json dans une page markdown, il suffit de l'appeler ainsi : `{{ file }}`. Le template jinja se nomme comme le fichier source json. Il s'agit d'une liste.

Par exemple, si l'on a un fichier nommé `courses.json` ainsi : 

```
[
 { "fruit": "pêche",
   "legume": "courgette",
}
]
```

Je le déclare dans mkdocs.yml :

```
plugins:
  - jinson:
      json_path: 
        - 'docs/courses.json'
```

et le récupère dans un page markdown ainsi :

```
{{ courses.fruit }}
// résultat : pêche
```

