from mkdocs import plugins, config
import json
import jinja2
import mkdocs
from mkdocs.config import Config
from mkdocs.plugins import BasePlugin

class getJinson(plugins.BasePlugin):

    config_scheme = (
        ('json_path', config.config_options.Type(list, default='')),
    )

    def on_page_markdown(self, markdown, page, config, files):
     

        env = jinja2.Environment(undefined=jinja2.DebugUndefined)
 
        md_template = env.from_string(markdown)
        allJson = self.config['json_path'];
        dict = {}
        for oneJson in allJson :
            #print(oneJson)
            jsonName = oneJson.split('/')[-1]
            jsonName = jsonName.split('.json')[0]
            jsonContent= open(oneJson, 'r')
            dict[jsonName] = json.load(jsonContent)

        return md_template.render(json=dict)

    def on_template_context(self, context, template_name, config):
        print(context)
        return context


